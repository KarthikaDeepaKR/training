1.Why except iterator() method in iterable interface, are not neccessary to define in the implemented class?
  Answer: 
        The Java Iterable interface has three methods of which only one needs to be implemented. 
 	The other two have default implementations. Here is the the definition of the Iterable interface:
	
2.Will the following class compile? If not, why?
	public final class Algorithm {
    	public static <T> T max(T x, T y) {
        	return x > y ? x : y;
    	}
	}
Answer: No. The greater than (>) operator applies only to primitive numeric types.
	
3.Write a generic method to find the maximal element in the range [begin, end) of a list.
	Answer: 
	
	public final class Algorithm {
    	public static <T extends Object & Comparable<? super T>>
        	T max(List<? extends T> list, int begin, int end) {

        		T maxElem = list.get(begin);

        	for (++begin; begin < end; ++begin)
            	if (maxElem.compareTo(list.get(begin)) < 0)
                	maxElem = list.get(begin);
        	return maxElem;
    	}
	}
	
4.Generic method to exchange the positions of two different elements in an array.
	Answer:
	public final class Algorithm {
    	public static <T> void swap(T[] a, int i, int j) {
        	T temp = a[i];
        	a[i] = a[j];
        	a[j] = temp;
    	}
	}

