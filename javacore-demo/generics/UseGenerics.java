/*5. What will be the output of the following program?
public class UseGenerics {
    public static void main(String[] args){  
        MyGen<Integer> m = new MyGen<Integer>();  
        m.set("merit");
        System.out.println(m.get());
    }
}
class MyGen<T>
{
    T var;
    void  set(T var)
    {
        this.var = var;
    }
    T get()
    {
        return var;
    }
}

 1.Requirements : 
     - What will be the output of the following program?
 2.Entities :
 	 - UseGenerics.
 3.Function Declaration :
 	 - public static void main(String[] args)
 4.Jobs To Be Done:
 		1.Creating the CountSpecificProperty class
 		2.Creating the count method which returns the count of odd numbers present in a list.
 		3.Creating the main method and create a list reference.
 		4.Adding elements inside a list.
 		5.Calling a count method and printing the number of odd numbers.
*/
package com.training.java.generics;
public class UseGenerics {
    public static void main(String[] args){  
        MyGen<Integer> print = new MyGen<Integer>();  
        print.set("merit");
        System.out.println(print.get());
    }
}
class MyGen<T>
{
    T var;
    void  set(String var)
    {
        //this.var =var;
        this.var =(T) var;
    }
    T get()
    {
        return var;
    }
}
