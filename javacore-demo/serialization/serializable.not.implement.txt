 - What happens if one of the members in a class does not implement Serializable interface?

Answer:
    When you try to serialize an object which implements Serializable
interface, incase if the object includes a reference of an non
serializable object then NotSerializableException will be thrown.