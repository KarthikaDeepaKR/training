/*Requirements
   - Program to print difference of two numbers using lambda expression and the single method interface
Entities
   - SetExample
Function Declaration
    public static void main(String[] args)
Jobs to be done
   1.Create a class and declaring main.
   2.Inside the main creating set called colours and adding 10 colours using add() method and printing the added set.
   3.Creating another set of newCars and adding 4 newCars using addAll() method adding colours and newCars sets and after using 
removeAll() method removing added newCars from colours set.
   4.Display the set using while loop with hasNext() and forEach.
   5.Checking the set with contain() method to set containing or not and to check set is empty or not using isEmpty() method.
*/
package com.training.java.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetExample {

	public static void main(String[] args) {
		
		//Creating a set and adding 10 elements to it.
		Set<String> colours = new HashSet<>();
        colours.add("black");
        colours.add("white");
        colours.add("pink");
        colours.add("orange");
        colours.add("Indigo");
        colours.add("green");
        colours.add("brown");
        colours.add("blue");
        colours.add("violet");
        colours.add("purple");
        System.out.println("Display Set:\n" + colours );
        
        //Creating another set with some elements.
        Set<String> newCars = new HashSet<>();
        newCars.add("Nano");
        newCars.add("Benz");
        newCars.add("Audi");
        newCars.add("BMW");
        
        //Perform addAll() method and removeAll() to the set.
        colours.addAll(newCars);
        System.out.println("\t-------------addAll() method-------------------- ");
        System.out.println(colours);
        System.out.println("\t-------------removeAll() method-------------------- ");
        colours.removeAll(newCars);
        System.out.println(colours);
        newCars.clear();
        
        
      //Displaying all the set elements using Iterator interface
        Iterator<String> car = colours.iterator();
        System.out.println("\t------------------Using While loop---------------------");
        while (car.hasNext()) {
            System.out.println(car.next());
        }

        //Displaying all the set elements using ForEach
        System.out.println("\t-------------Using ForEach---------------");
        colours.forEach(System.out::println);

        //contains()
        System.out.println("\t------------------contains() method---------------------");
        System.out.println("Checks colour set contains red " + colours.contains("red"));

        //isEmpty()
        System.out.println("\t------------------isEmpty() method---------------------");
        System.out.println("Checks newCars is empty or not? " + newCars.isEmpty());
	}

}