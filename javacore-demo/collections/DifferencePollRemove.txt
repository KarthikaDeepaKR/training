- what is the difference between poll() and remove() method of queue interface?
 
 Answer:
   
    * poll() and remove() method from Queue is used to remove the object and returns the head of the queue.
    * However If Queue is empty() then a call to remove() method will throw Exception, whereas a call to poll() method returns null.
	
(i)remove()
   > Retrieves and removes the head of this queue. This method differs from poll only in that it throws an exception if this queue is empty.
Returns:
   > the head of this queue   
Throws:
   > NoSuchElementException - if this queue is empty   
   
(ii)poll()
   > Retrieves and removes the head of this queue, or returns null if this queue is empty.
Returns:
   > the head of this queue, or null if this queue is empty   
   
   
   