/*
 Requirements:
   - Private Constructor with main class. 
 Entity:
   - Private Constructor
 Function Declaration:
   - public static void main(String[] args)
 Jobs to be done:
   1. Create an example for a Parameterized Constructor.
   2. Display the output.
*/
package com.training.java.reflections;
public class Vehicle {

    int wheels;

    // constructor accepting single value
    private Vehicle(int wheels){
        this.wheels = wheels;
        System.out.println(wheels + " wheeler vehicle created.");
    }

    public static void main(String[] args) {

        // calling the constructor by passing single value
        Vehicle v1 = new Vehicle(2);
        Vehicle v2 = new Vehicle(3);
        Vehicle v3 = new Vehicle(4);
    }
}