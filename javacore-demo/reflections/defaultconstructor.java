/*
 Requirements:
   - Private Constructor with main class. 
 Entity:
   - Private Constructor
 Function Declaration:
   - public static void main(String[] args)
 Jobs to be done:
   1. Create an example for a Default Constructor.
   2. Display the output.
*/

package com.training.java.reflections;
public class DefaultConstructor {

    int a;
    boolean b;

    public static void main(String[] args) {

        // A default constructor is called
        DefaultConstructor obj = new DefaultConstructor();

        System.out.println("a = " + obj.a);
        System.out.println("b = " + obj.b);
    }
}