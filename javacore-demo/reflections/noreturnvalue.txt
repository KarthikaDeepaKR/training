Why does constructors do not return any values?
   
   the reason the constructor doesn't return a value is because it's not called directly by your code. 
   It's called by the memory allocation and object initialization code in the runtime.