/*
 Requirements:
   - Private Constructor with main class. 
 Entity:
   - Private Constructor
 Function Declaration:
   - public static void main(String[] args)
 Jobs to be done:
   1. Create an example for a No-Arg Constructor.
   2. Display the output.
*/
package com.training.java.reflections;
public class Main {

   int i;

   // constructor with no parameter
   private NoArgConstructor(){
       i = 100;
       System.out.println("Object created and i = " + i);
   }

   public static void main(String[] args) {

       // calling the constructor without any parameter
       NoArgConstructor obj = new NoArgConstructor();
   }
}