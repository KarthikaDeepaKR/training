9)//THROW:
/*
 Requirements:
   -throw with example. 
 Entity:
   - ExampleThrow
 Function Declaration:
   - public static void main(String[] args)
 Jobs to be done:
    1.If the value given is equal to the value passed in if condition ,it throw an exception.//user-defined.
    2.Otherwise,else part will be executed.
    3.Display the output.
*/
package com.training.java.exception;
public class ExampleThrow{  
   void checkAge(int age){  
	if(age<18)  
	   throw new ArithmeticException("Not Eligible for voting");  
	else  
	   System.out.println("Eligible for voting");  
   }  
   public static void main(String[] args){  
	ExampleThrow obj = new ExampleThrow();
	obj.checkAge(13);  
	System.out.println("End Of Program");  
   }  
}