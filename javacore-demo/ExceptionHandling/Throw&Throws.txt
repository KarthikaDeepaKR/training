9)DIFFERENCE BETWEEN THROWS AND THROW:
  1)Syntax wise:
        -->throw is followed by an instance of Exception class.
              Example:
          throw new ArithmeticException("Arithmetic Exception");//user-defined
          
        -->throws is followed by exception class names.
              Example:
          throws ArithmeticException;
  2)Throw keyword is used in the method body to throw an exception,while throws is used in method signature to declare the exceptions.
              Example:Throw
                   void myMethod() {
   try {
      //throwing arithmetic exception using throw
      throw new ArithmeticException("Something went wrong!!");
   } 
   catch (Exception exp) {
      System.out.println("Error");
   }
}
              Example:Throws
                   void sample() throws ArithmeticException{
   //Statements
}

  3)Can throw one exception at a time but can handle multiple exceptions by declaring them using throws keyword.

