3. Compare the checked and unchecked exception.

   Checked exceptions :
                *Checked exceptions are the exceptions which are known to compiler. 
                *These exceptions are checked at compile time only. 
 *These exceptions are also called compile time exceptions. 
 *Because, these exceptions will be known during compile time.
 *java.lang.Exception is checked exception.
 *Example:
          SQLException, IOException, ClassNotFoundException.

   Unchecked exceptions :
                *Unchecked exceptions are those exceptions which are not at all known to compiler.
 *These exceptions occur only at run time. 
 *These exceptions are also called as run time exceptions.
 *All sub classes of java.lang.RunTimeException and java.lang.Error are unchecked exceptions.
                *Example:
                         NullPointerException, ArrayIndexOutOfBoundsException, ArithmeticException, IllegalArgumentException, NumberFormatException