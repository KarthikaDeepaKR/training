9)//THROWS:
/*
 Requirements:
   -throws with example. 
 Entity:
   - ExampleThrows
 Function Declaration:
   - public static void main(String[] args)
 Jobs to be done:
    1. Execute the try block.
    2. When an exception occurs in try block ,then go to the catch block.
    3. Check the exception, which are known to run time.
    4. Display the output.
*/
package com.training.java.exception;
public class ExampleThrows{  
   int division(int a, int b) throws ArithmeticException{  
	int t = a/b;
	return t;
   }  
   public static void main(String[] args){  
	ExampleThrows obj = new ExampleThrows();
	try{
	   System.out.println(obj.division(15,0));  
	}
	catch(ArithmeticException e){
	   System.out.println("You shouldn't divide number by zero");
	}
   }  
}