/*Requirements:
   Create a list
      =>Add 10 values in the list.
Entity:
   ListExample
Function Declaration:
   public static void main(String[] args)
Jobs to be done:
 1.Creating a new list using the syntax.
 2.Adding 10 values to the created list using add().
 3.Display the output.
*/
package com.training.java.list;
import java.util.*;  
public class ListExample{  
   public static void main(String[] args){  
      ArrayList<String> list1 = new ArrayList<String>();  //Creating new List
      list1.add("Apple");
      list1.add("Tomato");
      list1.add("Lime");
      list1.add("Papaya");
      list1.add("Orange");
      list1.add("Banana");
      list1.add("Ant");
      list1.add("Lion");
      list1.add("Parrot");
      list1.add("Owl");        // Adding 10 values in list
     
  
      //displaying elements
      System.out.println(list1);
 
   }  
}