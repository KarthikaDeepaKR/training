/*Requirement
  Convert list to set
Entity:
  ListToSet 
Function Declaration:
   public static void main(String[] args)
Jobs to be done:
   1.Creating a Generic function to convert list to set.
   2.Create a set from the List.
   3.Create a stream of integers.
   4.Print the List and Convert List to stream.
   5.Print the set.
*/
package com.training.java.list;
import java.util.*; 
  
public class ListToSet { 
  
    // Generic function to convert list to set 
    public static <T> Set<T> convertListToSet(List<T> list) 
    { 
        // create a set from the List 
        return new HashSet<>(list); 
    } 
  
    public static void main(String args[]) 
    { 
  
        // Create a stream of integers 
        List<String> list = Arrays.asList("Have", 
                                          "a", 
                                          "niceday", 
                                          "my dear friends", 
                                          "Happy", 
                                          "Learning of java"); 
  
        // Print the List 
        System.out.println("List: " + list); 
  
        // Convert List to stream 
        Set<String> set = convertListToSet(list); 
  
        // Print the Set 
        System.out.println("Set from List: " + set); 
    } 
} 