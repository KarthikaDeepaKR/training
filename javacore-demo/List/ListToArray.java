/*Requirements
    Convert list to array
Entity:
    ListToArray 
Function Declaration:
   public static void main(String[] args)
Jobs to be done:
   1.Creating a list and adding elements in list.
   2.Converting list to array using toArray method.
*/
package com.training.java.list;

import java.util.*; 

public class ListToArray { 
    public static void main(String[] args) 
    { 
        List<String> list = new LinkedList<String>(); 
        list.add("Keep"); 
        list.add("Learning"); 
        list.add("Something"); 
        list.add("new"); 
  
        String[] arr = list.toArray(new String[0]); 
  
        for (String x : arr) 
            System.out.print(x + " "); 
    } 
} 