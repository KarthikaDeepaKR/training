/*Requirement
    RetainAll with example.
Entity:
  RetainAll 
Function Declaration:
   public static void main(String[] args)
Jobs to be done:
   1.Creating an empty array list and add values in the created list.
   2.Creating another array list and add values in the created list.
   3.Applying retainAll() method to basket passing fruits as parameter
*/
package com.training.java.list;
import java.util.ArrayList; 
public class RetainAll { 
    public static void main(String[] args) 
    { 
        // Creating an empty array list 
        ArrayList<String> fruits = new ArrayList<String>(); 
  
        // Add values in the fruits list. 
        fruits.add("apple"); 
        fruits.add("kiwi"); 
        fruits.add("berry"); 
  
        // Creating another array list 
        ArrayList<String> basket = new ArrayList<String>(); 
  
        // Add values in the basket list. 
        basket.add("apple"); 
        basket.add("mango"); 
        basket.add("kiwi"); 
        basket.add("cherry"); 
  
        // Before Applying method print both lists 
        System.out.println("fruits Contains :" + fruits); 
        System.out.println("basket Contains :" + basket); 
  
        // Apply retainAll() method to basket passing fruits as parameter 
        basket.retainAll(fruits); 
  
        // Displaying both the lists after operation 
        System.out.println("\nAfter Applying retainAll()"+ 
        " method to basket\n"); 
        System.out.println("fruits Contains :" + fruits); 
        System.out.println("basket Contains :" + basket); 
    } 
} 