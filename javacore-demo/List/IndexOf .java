/*Requirement:
   Find the index of some value with indexOf()
Entity:
   IndexOf 
Function Declaration:
   public static void main(String[] args)
Jobs to be done:
 1.Creating a new list.
 2.Adding the values to the created list.
 3.This method helps to find the index of a particular elements in a list.
 4.This method returns -1 if the specified element is not present in the list.
 5.Display the output.
*/

package com.training.java.list;
import java.util.ArrayList;
public class IndexOf {
  public static void main(String[] args) {
      ArrayList<String> al = new ArrayList<String>();
      al.add("AB");
      al.add("CD");
      al.add("EF");
      al.add("GH");
      al.add("IJ");
      al.add("KL");
      al.add("MN");

      System.out.println("Index of 'AB': "+al.indexOf("AB"));
      System.out.println("Index of 'KL': "+al.indexOf("KL"));
      System.out.println("Index of 'AA': "+al.indexOf("XX"));
      System.out.println("Index of 'EF': "+al.indexOf("EF"));
  }
}