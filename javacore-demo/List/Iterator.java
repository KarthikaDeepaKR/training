/*Requirement
   Print the values in the list using
        -Iterator
Entity:
  Iterator
Function Declaration:
   public static void main(String[] args)
Jobs to be done:
   1.Creating a new list.
   2.Adding the values to the created list.
   3.Display the output using the Iterator method.//removes special characters.
*/
package com.training.java.list;
import java.util.*; 
  
public class Iterator { 
  
    public static void main(String[] args) 
    { 
        // Create and populate the list 
        ArrayList<String> list 
            = new ArrayList<>(); 
  
        list.add("Java"); 
        list.add("is"); 
        list.add(" a"); 
        list.add("Programming"); 
        list.add("Language"); 
        list.add("easy"); 
        list.add("to"); 
        list.add("learn"); 
  
        // Displaying the list 
        System.out.println("The list is: \n" + list); 
  
        // Create an iterator for the list using iterator() method 
        java.util.Iterator<String> iter = list.iterator(); 
  
        // Displaying the values after iterating through the list 
        System.out.println("\nThe iterator values of list are: "); 
        while (iter.hasNext()) { 
            System.out.print(iter.next() + " "); 
        } 
    } 
}