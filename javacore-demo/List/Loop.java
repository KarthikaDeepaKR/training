/*Requirement
   Print the values in the list using
        - For loop
        - For Each
Entity:
  Loop 
Function Declaration:
   public static void main(String[] args)
Jobs to be done:
   1.Creating a new list.
   2.Adding the values to the created list.
   3.Display the output using the for and foreach loop.
*/
package com.training.java.list;
public class Loop {
   public static void main(String[] args) {
      int[] intarr = { 1,2,3,4};
      forDisplay(intarr);
      foreachDisplay(intarr);
   }
   public static void forDisplay(int[] a) {  
      System.out.println("Display an array using for loop");
      for (int i = 0; i < a.length; i++) {
         System.out.print(a[i] + " ");
      }
      System.out.println();
   }
   public static void foreachDisplay(int[] data) {
      System.out.println("Display an array using foreach loop");
      for (int a  : data) {
         System.out.print(a+ " ");
      }
   }
}