/*Requirement:
   Find the index of some value with lastindexOf()
Entity:
   LastIndexOf 
Function Declaration:
   public static void main(String[] args)
Jobs to be done:
 1.Creating a new list.
 2.Adding the values to the created list.
 3.This method returns the index of last occurrence of the specified element in the list.
 4.This method returns -1 if the specified element is not present in the list.
 5.Display the output.
*/
package com.training.java.list;
import java.util.ArrayList;
public class LastIndexOf {
  public static void main(String[] args) {
      //ArrayList of Integer Type
      ArrayList<Integer> al = new ArrayList<Integer>();
      al.add(1);
      al.add(11);
      al.add(111);
      al.add(2);
      al.add(22);
      al.add(222);
      al.add(3);
      al.add(33);
      al.add(333);
      al.add(40);
      al.add(500);

      System.out.println("Last occurrence of element 500 : "+al.lastIndexOf(500));
      System.out.println("Last occurrence of element 22 : "+al.lastIndexOf(22));
      System.out.println("Last occurrence of element 3 : "+al.lastIndexOf(3));
      System.out.println("Last occurrence of element 6 : "+al.lastIndexOf(6));
   }
}