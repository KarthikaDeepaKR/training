retainAll :-    
  The retainAll() method of ArrayList is used to remove all the array list’s 
elements that are not contained in the specified collection or retains all matching 
elements in the current ArrayList instance that match all elements from the Collection list passed as a parameter to the method.