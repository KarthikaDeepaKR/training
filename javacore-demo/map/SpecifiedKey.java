/*Write a Java program to copy all of the mappings from the specified map to another map?
1.Requirements
   - Java program to copy all of the mappings from the specified map to another map.
2.Entities
   - SpecifiedKey
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as SpecifiedKey and declaring the main.
   2.In the class main creating object for HashMap with integer and String called cars.
   3.Adding the values using put() method and creating another hashMap cars1 for copying from cars to cars1 using putAll() method.
   4.Printing the copied hashMap cars1.
*/

package com.training.java.map;
import java.util.HashMap;

public class SpecifiedKey {

   public static void main(String args[]) {
	   
	  //Creating hashMap 
      HashMap<Integer, String> cars = new HashMap<Integer, String>();

      /*Adding elements to HashMap*/
        cars.put(1,"BMW");
        cars.put(2,"Audi");
        cars.put(3,"ferrari");
        cars.put(4,"Honda");
        cars.put(5,"Infiniti");
        cars.put(6,"Jaguar");
      
      HashMap<Integer, String> cars1 = new HashMap<Integer, String>();
      
      
      //Copy all of the mappings from the specified map to another map
      cars1.putAll(cars);
      System.out.println(cars1);
   }
}