/*Write a Java program to get the portion of a map whose keys range from a given key to another key?
1.Requirements
   - Java program to get the portion of a map whose keys range from a given key to another key.
2.Entities
   - GetPortion
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as GetPortion and declaring the main.
   2.In the class main creating object for TreeMap with integer and String called fruits.
   3.Selecting the portion of the TreeMap using subMap() method.
   3.Printing the Sub map key values.
*/

package com.training.java.map; 
import java.util.TreeMap; 

public class GetPortion {

	public static void main(String[] args) {
		
		 //Creating TreeMap and SortedMap
	      TreeMap<Integer, String> fruits = new TreeMap<Integer, String>();
	      
	      /*Adding elements to HashMap*/
	      fruits.put(2, "Banana");
	      fruits.put(20, "straw berry");
	      fruits.put(3, " green apple");
	      fruits.put(50, "orange");
	      fruits.put(90, "papaya");
	      
	     
		  System.out.println("-------------Sub map key-values------------------- \n" + fruits.subMap(20, 90));
	}

}