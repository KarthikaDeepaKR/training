/*Write a Java program to test if a map contains a mapping for the specified key?
1.Requirements
   - Java program to copy all of the mappings from the specified map to another map.
   - Count the size of mappings in a map
2.Entities
   - ContainsKey
3.Function Declaration
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as ContainsKey and declaring the main.
   2.In the class main creating object for HashMap with integer and String called fruits.
   3.Adding the values using put() method and using containsKey() method find the specified key value map is containing or not.
   4.Count the size of mappings in a map using size() method.
*/

package com.training.java.map;
import java.util.HashMap;

public class ContainsKey {
	 public static void main(String args[]) {
		   
		  //Creating hashMap 
	      HashMap<Integer, String> fruits = new HashMap<Integer, String>();

	      /*Adding elements to HashMap*/
	      fruits.put(2, "Banana");
	      fruits.put(20, "straw berry");
	      fruits.put(3, " green apple");
	      fruits.put(50, "orange");
	      fruits.put(90, "papaya");
	      
	      //Testing if a map contains a mapping for the specified key
	      System.out.println("---------Checking '20' key value map containing or not----------");
	      System.out.println(fruits.containsKey(20));
	      
	      System.out.println("---------Checking '26' key value map containing or not----------");
	      System.out.println(fruits.containsKey(26));
	      
	      //Count the size of mappings in a map
	      System.out.println("---------Count the size of mappings in a map----------");
	      System.out.println(fruits.size());
	      
	   }
}
