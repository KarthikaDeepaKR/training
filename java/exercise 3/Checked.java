/*package com.learning ;

import java.io.*;

class Checked {  
    public static void main(String args[]) 
   {
	FileInputStream fis = null;
	// This constructor FileInputStream throws FileNotFoundException which is a checked exception
    
	fis = new FileInputStream("L:/myfile.txt"); 
	int k; 

	// Method read() of FileInputStream class also throws a checked exception: IOException
         
	while(( k = fis.read() ) != -1) 
	{ 
		System.out.print((char)k); 
	} 

	// The method close() closes the file input stream it throws IOException
	
	fis.close(); 	
   }
}*/



package com.learning ;

import java.io.*;

class Checked {  
    public static void main(String args[]) throws IOException
   {
	FileInputStream fis = null;
	
	fis = new FileInputStream("L:/myfile.txt"); 
	int k; 

	while(( k = fis.read() ) != -1) 
	{ 
		System.out.print((char)k); 
	}
	
	fis.close(); 	
   }
}
