package com.learning;

public class MultipleCatchBlock {  
  
    public static void main(String[] args) {  
          
           try{    
                int a[]=new int[8];    
                //a[8] = 10 /0;
                System.out.println(a[18]);  
               }    
               catch(ArithmeticException e)  
                  {  
                   System.out.println("Arithmetic Exception occurs");  
                  }    
               catch(ArrayIndexOutOfBoundsException e)  
                  {  
                   System.out.println("ArrayIndexOutOfBounds Exception occurs");  
                  }    
               catch(Exception e)  
                  {  
                   System.out.println("Parent Exception occurs");  
                  }             
               System.out.println("print something");    
    }  
} 