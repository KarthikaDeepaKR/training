package com.learning;

public class Unchecked {

    public static void main(String[] args) {
        try{
            int number = 10 / 0 ;  
			/*dividing a integer with zero 
		    it should throw an arithematic exception
			*/
			System.out.println(number);
        }
        catch(Exception e){
            System.out.println("something is wrong");
        }
    }
 