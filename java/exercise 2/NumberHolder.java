/*
    Question :
	
    Given the following class, called NumberHolder,
    write some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.
	
	public class NumberHolder {
        public int anInt;
        public float aFloat;
    }
*/

/*  
    Requirements : To create an instance for the given class.
	               To initialize two member variables of the class with given values.
				   To display the value of each member variable.
*/

/*  
    Entities : public class NumberHolder 
*/

/* 
    Method Signature : public static void main(String[] args)
*/

/*
    Jobs to be done : 1. Declare two variables anInt and aFloat.
                      2. Inside the main function an object is created named holder for the class NumberHolder.
					  3. Values 12345 and 25.6f are initialized to variables anInt and aFloat using object holder.
					  4. The vlaues of anInt and aFloat is display.
*/
    
//Ans:

       public class NumberHolder {
              public static int anInt;
              public static Float aFloat;
			  
            public static void main(String[] args) {
              NumberHolder holder = new NumberHolder();
              holder.anInt = 12345;
              holder.aFloat = 25.6f;
              System.out.println(anInt);
              System.out.println(aFloat);
           }
       }