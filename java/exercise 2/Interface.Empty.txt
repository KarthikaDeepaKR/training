 Is the following interface valid?
    public interface Marker {}
	
	
Ans:
  
   Yes. Methods are not required.Here empty interfaces are used ,empty interfaces can be used as types and to mark classes without requiring
   any particular method implementations. java.io.Serializable is an example of a using empty interface .