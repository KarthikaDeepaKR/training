/*
    Question :
	
    print the type of the result value of following expressions
    - 100 / 24
    - 100.10 / 10
    - 'Z' / 2
    - 10.5 / 0.5
    - 12.4 % 5.5
    - 100 % 56
*/

/*  
    Requirements : To print the type of result of given expressions.
*/

/* 
    Entities : public class ReturnDatatype
 */
 
 /*
    Method Signature : public static void main(String[] args) 
 */
 
 /* 
    Jobs to be done : 1. Inside the main function initialize Objects a,b,c,d,e,f,g with the given expressions.
                      2. Display the type of the each initialized object using the function <Object>.getClass().getName().	
*/

//Ans:

      public class ReturnDatatype {
            public static void main(String[] args) {
            Object a = 123 / 2 ; 
            Object b = 100 / 24 ;
            Object c = 100.10 / 10;
            Object d = 'Z' / 2 ;
            Object e = 10.5 / 0.5 ;
            Object f = 12.4 % 5.5;
            Object g = 100 % 56;
            System.out.println(a.getClass().getName());
            System.out.println(b.getClass().getName());
            System.out.println(c.getClass().getName());
            System.out.println(d.getClass().getName());
            System.out.println(e.getClass().getName());
            System.out.println(f.getClass().getName());
            System.out.println(g.getClass().getName());
           }
       }