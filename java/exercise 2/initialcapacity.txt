What is the initial capacity of the following string builder?

StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.");

Ans:
 It's the length of the initial string + 16: 26 + 16 = 42.
