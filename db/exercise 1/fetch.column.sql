 create table `database` . `demo`
(
     fname varchar(20),
     age int,
     place varchar(20)
);

insert into `database` . `demo` values('John','23','chennai');
insert into `database` . `demo` values('ram','18','cbe');
insert into `database` . `demo` values('seetha','56','madurai');

select * from `database` . `demo`;
select * from `database` . `demo` where fname LIKE 'seetha';
select * from `database` . `demo`;

select * from `database` . `demo` where age LIKE '23';
