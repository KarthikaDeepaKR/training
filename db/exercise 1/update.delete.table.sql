CREATE TABLE `database`.`students` (
  `roll no` INT ,
  `fname` VARCHAR(45) ,
  `lastname` VARCHAR(45) ,
  `class` VARCHAR(45) ,
  `age` VARCHAR(45) 
  );

INSERT INTO `database`.students
VALUES(2,'bahu','','II',6);
INSERT INTO `database`.students
VALUES(3,'devi','kruba','II',6);
INSERT INTO `database`.students
VALUES(1,'anu','priya','II',6);

UPDATE `database`.students
SET lastname='shiva' ;

UPDATE `database`.students
SET lastname='aasik'
WHERE fname='bahu' ;

DELETE FROM `database`.students
WHERE fname='devi' ;

DELETE FROM `database`.students
