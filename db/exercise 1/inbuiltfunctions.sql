CREATE TABLE `database`.`inbuilt_table` (
  `id` INT ,
  `fname` VARCHAR(45),
  `age` INT ,
  `salary` INT 
);

insert into  `database`.`inbuilt_table`(id,fname,age,salary)
values ('2','a','23','20000');
insert into  `database`.`inbuilt_table`(id,fname,age,salary)
values ('9','p','29','25000');
insert into  `database`.`inbuilt_table`(id,fname,age,salary)
values ('78','a','23','20000');
insert into  `database`.`inbuilt_table`(id,fname,age,salary)
values ('7','t','20','15000');
insert into  `database`.`inbuilt_table`(id,fname,age,salary)
values ('17','m','45','35000');
insert into  `database`.`inbuilt_table`(id,fname,age,salary)
values ('67','bn','19','30000');
insert into  `database`.`inbuilt_table`(id,fname,age,salary)
values ('6','df','35','56000');

select * from  `database`.`inbuilt_table`;

select avg(salary)from `database`.`inbuilt_table`;
select max(salary)from `database`.`inbuilt_table`;
select min(salary)from `database`.`inbuilt_table`;
select sum(salary)from `database`.`inbuilt_table`;
select count(*)from `database`.`inbuilt_table`;

select * from  `database`.`inbuilt_table`;


