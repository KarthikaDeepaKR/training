CREATE TABLE `database` . `vehicles` (
    vehicleId INT,
    colour varchar(45),
	type varchar(45)
 );

ALTER TABLE `database` . `vehicles`
ADD model VARCHAR(45) ;

ALTER TABLE `database`. `vehicles` 
MODIFY colour VARCHAR(100) ;

ALTER TABLE `database` . `vehicles` 
CHANGE COLUMN vehicleId vehicleNo int;

ALTER TABLE `database` . `vehicles`
DROP COLUMN  type;