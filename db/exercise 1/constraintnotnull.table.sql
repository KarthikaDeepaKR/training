CREATE TABLE `database`.`student_table` (
  `reg_no` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `age` INT NOT NULL,
  `class` INT NOT NULL,
  `transport` VARCHAR(45) NOT NULL
);
INSERT INTO  `database`.`student_table`(
`reg_no`,  `name` ,`city`,`age`,`class`, `transport`)
VALUES (10,'RAJ','CBE','10',5,'cycle');
INSERT INTO `database`.`student_table`(
`reg_no`,  `name` ,`city`,`age`,`class`, `transport`)
VALUES (20,'renu','CBE','9',4,'bus');
INSERT INTO  `database`.`student_table`(
`reg_no`,  `name` ,`city`,`age`,`class`, `transport`)
VALUES (30,'karthi','CBE','10',5,'bus');
      
SELECT * FROM `database`.student_table;