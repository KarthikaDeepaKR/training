CREATE TABLE `database`.`employee table` (
  `emp_id` INT ,
  `first_name` VARCHAR(45) ,
  `last_name` VARCHAR(45) ,
  `age` INT,
  `salary` INT
  );

CREATE TABLE `database`.`department table` (
  `department_id` INT,
  `department_name` VARCHAR(45),
  `department_location` VARCHAR(45)
  );

DROP TABLE `database` . `department table`;
RENAME TABLE `database` .`employee table` TO `database` . `emp_table`;
