select college.code
	  ,college.name
      ,(select distinct university_name
          from university
         where university.univ_code=college.univ_code limit 1)
			  ,college.city
			  ,college.state
			  ,college.year_opened
	  ,(select distinct department.dept_name
		  from department
		 where department.dept_name = 'CSE'
			or department.dept_name = 'IT' 
			or department.univ_code = college.univ_code limit 1)
  from college
	  ,university
 where college.univ_code = university.univ_code      
  
	