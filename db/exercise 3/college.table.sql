CREATE TABLE education.college (id INT AUTO_INCREMENT
			,code CHAR(4)
			,name VARCHAR(100)
			,univ_code CHAR(4)
			,city VARCHAR(50)
			,state VARCHAR(50)
			,year_opened YEAR
			,PRIMARY KEY (id)
			,INDEX univ_idx (univ_code ASC) VISIBLE
			,CONSTRAINT univ
		     FOREIGN KEY (univ_code)
		     REFERENCES education.university(univ_code)
			 ON DELETE NO ACTION
			 ON UPDATE NO ACTION);
				
insert into education.college(id,code,name,univ_code,city,state,year_opened)
values('501', 'BDA', 'BUMC', 'B', 'trichy', 'tamilnadu', '1960');
insert into education.college(id,code,name,univ_code,city,state,year_opened)
values('510', 'OIC', 'BKN', 'K', 'madurai', 'tamilnadu', '1969');
insert into education.college(id,code,name,univ_code,city,state,year_opened)
values('618', 'CPU', 'KPR', 'A', 'cbe', 'tamilnadu', '2009');
insert into education.college(id,code,name,univ_code,city,state,year_opened)
values('816', 'CNN', 'sastra', 'S', 'thanjavur', 'tamilnadu', '1984');
insert into education.college(id,code,name,univ_code,city,state,year_opened)
values('491', 'REV', 'HICAS', 'Y', 'cbe', 'tamilnadu', '1998');