SELECT employee.id AS 'employee_id'
      ,employee.name AS 'employee_name'
      ,employee.dob
      ,employee.email
      ,employee.phone
      ,designation.name
      ,designation.grade
      ,college.name AS college_name
      ,university.univ_code
      ,university.university_name
  FROM employee
      ,college
      ,university
      ,designation
 WHERE college.id = employee.college_id
   AND college.univ_code = university.univ_code
   AND designation.id = employee.desig_id
   AND university.university_name = 'SASTRA'
 ORDER BY college.name,designation.grade