CREATE TABLE education.college_department(
			 cdept_id INT NOT NULL
            ,udept_code CHAR(4) NULL
            ,college_id INT NULL
            ,PRIMARY KEY (cdept_id)
            ,INDEX college_id_idx (college_id ASC) VISIBLE,
            ,CONSTRAINT udept_code
             FOREIGN KEY (udept_code)
			 REFERENCES education.department (univ_code)
			 ON DELETE NO ACTION
             ON UPDATE NO ACTION,
             CONSTRAINT college_id
             FOREIGN KEY (college_id)
             REFERENCES education.college (id)
			 ON DELETE NO ACTION
             ON UPDATE NO ACTION
);

insert into education.college_department (cdept_id,udept_code,college_id)
values ('460', 'A', '500');
insert into education.college_department (cdept_id,udept_code,college_id)
values ('560', 'B', '816');
insert into education.college_department (cdept_id,udept_code,college_id)
values ('700', 'S', '618');
insert into education.college_department (cdept_id,udept_code,college_id)
values ('450', 'Y', '120');
insert into education.college_department (cdept_id,udept_code,college_id)
values ('167', 'K', '491');


CREATE TABLE education.student (
             id INT NOT NULL
            ,roll_number CHAR(8) NOT NULL
            ,name CHAR(8) NOT NULL
            ,dob DATE NOT NULL
			,gender CHAR(1) NOT NULL
			,email VARCHAR(50) NOT NULL
            ,phone BIGINT NOT NULL
            ,address VARCHAR(200) NOT NULL
            ,academic_year YEAR NOT NULL
            ,cdept_id INT NULL
            ,college_id INT NULL
            ,PRIMARY KEY (id)
			,INDEX collegeid_idx (college_id ASC) VISIBLE
			,INDEX cdept_idx (cdept_id ASC) VISIBLE
			,CONSTRAINT cdept
             FOREIGN KEY (cdept_id)
             REFERENCES education.college_department (cdept_id)
             ON DELETE NO ACTION
             ON UPDATE NO ACTION,
  CONSTRAINT collegeid
    FOREIGN KEY (college_id)
    REFERENCES education.college (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
insert into education.student (id,roll_number,name,dob,gender,email,phone,address,academic_year,cdept_id,college_id)
values ('90', '15', 'karthika', '2001-04-27', 'F', 'abc@gmail.com', '1234567', 'barathi nagar', '2020', '167', '120');
insert into education.student (id,roll_number,name,dob,gender,email,phone,address,academic_year,cdept_id,college_id)
values ('19', '30', 'awin', '2000-10-19', 'M', 'xyz@gmail.com', '246810', 'annai nagar', '2020', '450', '510');
insert into education.student (id,roll_number,name,dob,gender,email,phone,address,academic_year,cdept_id,college_id)
values ('29', '45', 'teju', '1999-01-25', 'F', 'idk@gmail.com', '3691218', 'besant nagar', '2020', '460', '816');
insert into education.student (id,roll_number,name,dob,gender,email,phone,address,academic_year,cdept_id,college_id)
values ('39', '65', 'henna', '2000-03-16', 'F', 'lmn@gmail.com', '48121620', 'anna nagar', '2020', '560', '500');
insert into education.student (id,roll_number,name,dob,gender,email,phone,address,academic_year,cdept_id,college_id)
values ('49', '85', 'raju', '2001-09-29', 'M', 'uvw@gmail.com', '5101520', 'kandhan nagar', '2020', '700', '501');








