SELECT * FROM education.semester_result;

insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('9', '250', '3', 'A', '4', '8.5', '2020-01-01');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('19', '350', '2', 'B', '3', '5.5', '2020-02-05');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('29', '550', '4', 'C', '4', '7', '2020-03-01');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('39', '900', '1', 'A', '1', '9.8', '2020-02-01');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('49', '750', '5', 'U', '4', '3', '2020-02-01');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('147', '900', '3', 'A', '4', '7.5', '2020-03-01');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('234', '250', '2', 'B', '3', '2.5', '2020-02-15');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('678', '550', '5', 'C', '4', '7', '2020-03-01');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('789', '550', '3', 'A', '1', '8', '2020-01-01');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('900', '750', '3', 'U', '4', '2.99', '2020-02-06');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('120', '250', '2', 'A', '3', '6.75', '2020-01-01');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('165', '550', '4', 'B', '3', '7.5', '2020-02-05');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('250', '550', '4', 'C', '4', '7', '2020-03-01');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('561', '900', '1', 'A', '3', '7.99', '2020-03-09');
insert into `education`.`semester_result`(stud_id,syllabus_id,semester,grade,credits,gpa,result_date)
values ('987', '750', '5', 'A', '4', '8.79', '2020-02-01');