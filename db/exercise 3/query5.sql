SELECT student.roll_number
      ,student.name
	  ,semester_result.grade
	  ,semester_result.credits
  FROM student
	  ,semester_result  
 WHERE student.id = semester_result.stud_id 
 ORDER BY semester_result.semester