CREATE TABLE education.syllabus (
             id INT NOT NULL
			,cdept_id INT NULL
            ,syllabus_code CHAR(4) NOT NULL
            ,syllabus_name VARCHAR(100) NOT NULL
            ,PRIMARY KEY (id)
            ,INDEX cdept_id_idx (cdept_id ASC) VISIBLE,
  CONSTRAINT cdept_id
  FOREIGN KEY (cdept_id)
  REFERENCES education.college_department (cdept_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION);
    
insert into education.syllabus (id,cdept_id,syllabus_code,syllabus_name)
values ('250', '167', 'abcd', 'engimaths');
insert into education.syllabus (id,cdept_id,syllabus_code,syllabus_name)
values ('550', '450', 'hike', 'engimaths2');
insert into education.syllabus (id,cdept_id,syllabus_code,syllabus_name)
values ('750', '460', 'nike', 'engigraphics');
insert into education.syllabus (id,cdept_id,syllabus_code,syllabus_name)
values ('900', '560', 'pond', 'engichem');
insert into education.syllabus (id,cdept_id,syllabus_code,syllabus_name)
values ('350', '700', 'sing', 'engiphy');

CREATE TABLE education.semester_result (
             stud_id INT NULL
            ,syllabus_id INT NULL
            ,semester TINYINT NOT NULL
			,grade VARCHAR(45) NOT NULL
            ,credits FLOAT NOT NULL
		    ,result_date DATE NOT NULL
			,INDEX stud_id_idx (stud_id ASC) VISIBLE
			,INDEX syllabus_id_idx (syllabus_id ASC) VISIBLE,
  CONSTRAINT stud_id
  FOREIGN KEY (stud_id)
  REFERENCES education.student (id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT syllabus_id
  FOREIGN KEY (syllabus_id)
  REFERENCES education.syllabus (id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION);
    
insert into education.semester_result (stud_id,syllabus_id,semester,grade,credits,result_date)
values ('9', '250', '3', 'A', '4', '2020-01-01');
insert into education.semester_result (stud_id,syllabus_id,semester,grade,credits,result_date)
values ('19', '350', '2', 'B', '3', '2020-02-05');
insert into education.semester_result (stud_id,syllabus_id,semester,grade,credits,result_date)
values ('29', '550', '4', 'C', '4', '2020-03-01');
insert into education.semester_result (stud_id,syllabus_id,semester,grade,credits,result_date)
values ('39', '900', '1', 'A', '1', '2020-02-01');
insert into education.semester_result (stud_id,syllabus_id,semester,grade,credits,result_date)
values ('49', '750', '5', 'U', '4', '2020-01-05');