ALTER TABLE employee
CHANGE COLUMN name name VARCHAR(100) NULL ,
CHANGE COLUMN dob dob DATE NULL ,
CHANGE COLUMN email email VARCHAR(50) NULL ,
CHANGE COLUMN phone phone BIGINT NULL ;

INSERT INTO employee (id,college_id,cdept_id,desig_id)
VALUES ('3500', '510', '167', '100');
INSERT INTO employee (id,college_id,cdept_id,desig_id)
VALUES ('5500', '120', '450', '150'); 
INSERT INTO employee (id,college_id,cdept_id,desig_id)
VALUES ('1900', '491', '460', '170');
INSERT INTO employee (id,college_id,cdept_id,desig_id)
VALUES ('4090', '120', '700', '150'); 
INSERT INTO employee (id,college_id,cdept_id,desig_id)
VALUES ('5050', '491', '560', '170');   


SELECT designation.name AS 'designation'
      ,designation.grade
      ,college.name AS 'college_name'
      ,department.dept_name
      ,university.university_name
  FROM employee
      ,college_department
      ,department
      ,university
      ,designation 
      ,college
 WHERE employee.name is NOT NULL
   AND college.univ_code = university.univ_code 
   AND department.univ_code = university.univ_code
   AND college_department.college_id = college.id
   AND college_department.udept_code = department.dept_code
   AND employee.college_id = college.id
   AND employee.cdept_id = college_department.cdept_id 
   AND employee.desig_id = designation.id