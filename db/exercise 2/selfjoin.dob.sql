
create table database1.employee1(
e1empid int,
e1firstname varchar(10),
e1surname varchar(10),
e1dob date,
e1area varchar(10),
primary key(e1empid)
);
insert into database1.employee1 values('1','deepa','K','2001-04-27','cbe');
insert into database1.employee1 values('2','siva','G','2000-09-04','madurai');
insert into database1.employee1 values('3','sanju','Y','1999-07-23','cbe');
insert into database1.employee1 values('4','deepak','I','1995-10-29','chennai');
insert into database1.employee1 values('5','abi','A','2000-11-13','salem');
insert into database1.employee1 values('6','anu','T','1998-10-16','erode');
insert into database1.employee1 values('7','reka','K','2001-04-27','cbe');
insert into database1.employee1 values('8','durga','S','2000-12-30','chennai');
insert into database1.employee1 values('9','senthil','M','2001-12-19','cochin');
insert into database1.employee1 values('10','priya','P','1999-09-17','ariyalur');

create table database1.employee2(
e2empid int,
e2firstname varchar(10),
e2surname varchar(10),
e2dob date,
e2dateofjoining date,
e2annualsalary int,
e2area varchar(10),
primary key(e2empid)
);

insert into database1.employee2 values('11','aswini','R','2001-03-14','2020-02-01','300000','cbe');
insert into database1.employee2 values('12','kothai','N','2000-11-16','2019-02-25','208000','erode');
insert into database1.employee2 values('13','sakthi','L','2000-01-17','2019-03-01','2850000','salem');
insert into database1.employee2 values('14','jeni','S','2000-07-07','2019-07-20','270000','theni');
insert into database1.employee2 values('15','sherin','C','2000-08-07','2018-01-01','290000','madurai');
insert into database1.employee2 values('16','aasik','V','1999-04-27','2018-10-05','300000','mysore');
insert into database1.employee2 values('17','velu','B','1998-05-28','2018-09-05','250000','salem');
insert into database1.employee2 values('18','mani','G','1990-09-25','2014-04-01','350000','cbe');
insert into database1.employee2 values('19','vasu','S','2001-09-09','2020-01-30','290000','karur');
insert into database1.employee2 values('20','nivi','R','1995-09-10','2017-02-01','200000','namakal');

select * from database1.employee2;
select * from database1.employee1;

select e1firstname,
       e1area
  from database1.employee1,
       database1.employee2
 where e1area = e2area
 and e2firstname = 'velu';
 
 select e1empid,
        e1firstname,
        e1surname,
        e1dob,
        e1area
   from database1.employee1
  where e1dob = '2001-04-27'; 

