SELECT * FROM database1.employee;
ALTER TABLE `database1`.`employee` 
DROP FOREIGN KEY `deptid`;
ALTER TABLE `database1`.`employee`
DROP COLUMN `deptid`,
DROP INDEX `deptid`; 


ALTER TABLE `database1`.`employee`
CHANGE COLUMN `deptid` `deptid` int null;


ALTER TABLE `database1`.`employee`
ADD CONSTRAINT `deptid`
FOREIGN KEY (`deptid`)
REFERENCES `database1.department` (deptid);

insert into `database1`.`employee`(empid,firstname,surname,dob,dateofjoining,annualsalary,area)
values ('01','karthika','k','2001-04-27','2020-01-01','200000','erode');
insert into `database1`.`employee`(empid,firstname,surname,dob,dateofjoining,annualsalary,area)
values ('02','deepa','r','2000-07-24','2019-03-02','200000','cbe');
insert into `database1`.`employee`(empid,firstname,surname,dob,dateofjoining,annualsalary,area)
values('03','siva','p','1999-09-06','2019-04-03','200000','erode');
SELECT * FROM database1.employee;
select 
  empid,
  firstname,
  surname,
  dob,
  dateofjoining,
  annualsalary,
  area
  from `database1`.`employee`
 where deptid is null; 





