create table database1.employee(
empid int,
firstname varchar(30),
surname varchar(30),
dob date,
dateofjoining date,
annualsalary int,
primary key(empid)
);

create table database1.department(
deptid int,
deptname varchar(30),
primary key(deptid)
);

insert into database1.employee values('1','deepa','K','2001-04-27','2020-01-01','200000');
insert into database1.employee values('2','siva','G','2000-09-04','2019-09-01','270000');
insert into database1.employee values('3','sanju','Y','1999-07-23','2018-01-12','230000');
insert into database1.employee values('4','deepak','I','1995-10-29','2019-01-01','200000');
insert into database1.employee values('5','abi','A','2000-11-13','2020-01-01','210000');
insert into database1.employee values('6','anu','T','1998-10-16','2018-01-01','250000');
insert into database1.employee values('7','reka','K','2001-10-27','2019-01-10','200000');
insert into database1.employee values('8','durga','S','2000-12-30','2019-10-05','200000');
insert into database1.employee values('9','senthil','M','2001-12-19','2020-01-07','270000');
insert into database1.employee values('10','priya','P','1999-09-17','2019-03-03','280000');
insert into database1.employee values('11','aswini','R','2001-03-14','2020-02-01','300000');
insert into database1.employee values('12','kothai','N','2000-11-16','2019-02-25','208000');
insert into database1.employee values('13','sakthi','L','2000-01-17','2019-03-01','2850000');
insert into database1.employee values('14','jeni','S','2000-07-07','2019-07-20','270000');
insert into database1.employee values('15','sherin','C','2000-08-07','2018-01-01','290000');
insert into database1.employee values('16','aasik','V','1999-04-27','2018-10-05','300000');
insert into database1.employee values('17','velu','B','1998-05-28','2018-09-05','250000');
insert into database1.employee values('18','mani','G','1990-09-25','2014-04-01','350000');
insert into database1.employee values('19','vasu','S','2001-09-09','2020-01-30','290000');
insert into database1.employee values('20','nivi','R','1995-09-10','2017-02-01','200000');
insert into database1.employee values('21','madhu','M','1995-04-20','2018-01-09','220000');
insert into database1.employee values('22','sri','G','2001-09-29','2020-02-05','210000');
insert into database1.employee values('23','bavi','J','1990-12-19','2014-09-07','280000');
insert into database1.employee values('24','rohan','P','2000-09-08','2019-03-09','250000');
insert into database1.employee values('25','krish','I','2001-06-20','2020-03-01','230000');
insert into database1.employee values('26','vijay','R','2001-07-30','2019-10-01','210000');
insert into database1.employee values('27','deva','S','1999-04-13','2019-01-20','200000');
insert into database1.employee values('28','varsha','C','1993-10-03','2016-05-25','220000');
insert into database1.employee values('29','tamil','B','1994-07-28','2018-03-18','290000');
insert into database1.employee values('30','latha','A','1998-08-19','2020-02-01','300000');

select *from database1.employee;

insert into database1.department values('1','IT desk');
insert into database1.department values('2','finance');
insert into database1.department values('3','HR');
insert into database1.department values('4','recruitment');
insert into database1.department values('5','engineering');

select *from database1.department;

alter table database1.employee add column deptid int;
alter table database1.employee
add foreign key(deptid)
references  database1.department(deptid);

update database1.employee set deptid = 1 where  empid = 1;
update database1.employee set deptid = 2  where empid = 2;
update database1.employee set deptid = 3 where  empid = 3;
update database1.employee set deptid = 4 where  empid = 4;
update database1.employee set deptid = 5 where  empid = 5;
update database1.employee set deptid = 1 where  empid = 6;
update database1.employee set deptid = 2  where empid = 7;
update database1.employee set deptid = 3 where  empid = 8;
update database1.employee set deptid = 4 where  empid = 9;
update database1.employee set deptid = 5 where  empid = 10;
update database1.employee set deptid = 1 where  empid = 11;
update database1.employee set deptid = 2  where empid = 12;
update database1.employee set deptid = 3 where  empid = 13;
update database1.employee set deptid = 4 where  empid = 14;
update database1.employee set deptid = 5 where  empid = 15;
update database1.employee set deptid = 1 where  empid = 16;
update database1.employee set deptid = 2  where empid = 17;
update database1.employee set deptid = 3 where  empid = 18;
update database1.employee set deptid = 4 where  empid = 19;
update database1.employee set deptid = 5 where  empid = 20;
update database1.employee set deptid = 1 where  empid = 21;
update database1.employee set deptid = 2  where empid = 22;
update database1.employee set deptid = 3 where  empid = 23;
update database1.employee set deptid = 4 where  empid = 24;
update database1.employee set deptid = 5 where  empid = 25;
update database1.employee set deptid = 1 where  empid = 26;
update database1.employee set deptid = 2  where empid = 27;
update database1.employee set deptid = 3 where  empid = 28;
update database1.employee set deptid = 4 where  empid = 29;
update database1.employee set deptid = 1 where  empid = 1;
update database1.employee set deptid = 2  where empid = 2;
update database1.employee set deptid = 3 where  empid = 3;
update database1.employee set deptid = 4 where  empid = 4;
update database1.employee set deptid = 5 where  empid = 5;
update database1.employee set deptid = 5 where  empid = 30;

select * from database1.employee;
















