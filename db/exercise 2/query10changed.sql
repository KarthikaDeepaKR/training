SELECT * FROM database1.employee;
ALTER TABLE `database1`.`employee` 
DROP FOREIGN KEY `deptid`;
 
ALTER TABLE `database1`.`employee`
CHANGE COLUMN `deptid` `deptid` int null;


ALTER TABLE `database1`.`employee`
ADD CONSTRAINT deptid
FOREIGN KEY (deptid)
REFERENCES `database1`.`department` (deptid);


select 
  empid,
  firstname,
  surname,
  dob,
  dateofjoining,
  annualsalary,
  area
  from `database1`.`employee`
 where deptid is null; 
