/*
  Problem Statement
  	POJO Class for Address
  
  Entity
  	Address
  
  Method Signature
	  1.public Address(String street, String city, int postal_code);
	  2.public String getStreet();
	  3.public String getCity();
	  4.public int getPostalCode();
  
  Jobs to be Done
	  1.Create a id field of long type
	  2.Create a street field of String type
	  3.Create a city field of String type
	  4.Create a postal_code of int type
	  5.Create a public constructor take all the Address fields
	  6.Create a Street getter of String return type
	  7.Create a city getter of String return type
	  8.Create a postalCode getter of int type
  
  Pseudo Code
	  public class Address {
	 
	 	public static long id;
	 	public static String street;
	 	public static String city;
	 	public static int postal_code;
	 	
	 	public Address(String street, String city, int postal_code) {
	 		this.street = street;
	 		this.city = city;
	 		this.postal_code = postal_code;
	 	}
	 	
	 	public String getStreet() {
	 		return street;
	 	}
	 	
	 	public String getCity() {
	 		return city;
	 	}
	 	
	 	public int getPostalCode() {
	 		return postal_code;
	 	}
	 	
	 }

*/

package com.kpr.training.jdbc.model;

public class Address {

	public long id;
	public String street;
	public String city;
	public long postalCode;
	
	public Address(String street, String city, long postalCode) {
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
	}
	
	public Address(long id, String street, String city, long postalCode) {
		this.id = id;
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
	}
	

	public long getId() {
		return id;
	}
	
	public String getStreet() {
		return street;
	}
	
	public String getCity() {
		return city;
	}
	
	public long getPostalCode() {
		return postalCode;
	}
	
	public void setId(long idGot) {
		this.id = idGot;
	}
	
	public void setStreet(String streetGot) {
		this.street = streetGot;
	}
	
	public void setCity(String cityGot) {
		this.city = cityGot;
	}
	
	public void setPostalCode(long postalCode) {
		this.postalCode = postalCode;
	}
	
	@Override
	public String toString() {
		return new StringBuilder(" Address [id=").append(id)
				.append(", street = ")
				.append(street)
				.append(", city = ")
				.append(city)
				.append(", postalCode = ")
				.append(postalCode)
				.append("]")
				.append("\n")
				.toString();
	}
	
}
