/* 
    Question : What's wrong with the following program? And fix it.

    public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }
*/

/*  
    Requirements : To analyse the given code and debug it.
*/

/*  
    Entities : public class Rectangle
*/

/*  
    Method Signature :  public int area()	
	                    public static void main(String[] args)
*/

/*  
    Jobs to be done : 1. Two variables width and height are declared.
                      2. A method named area is defined.
					  3. A variable area is declared.
					  4. Inside the main function,a object myRect is created for class Rectangle.
					  5. Values 40 and 50 are assigned to width and height respectively using object myRect.
					  6. Variable area is displayed using object myRect.
*/
 


//Ans:

/*
   Class name is inappropriate.
   An instance has not been created for object myrect.
   Function area() to calculate area has not been created.
*/
package com.training.java.exercise
   public class Rectangle {
   int width, height; 
     public int area() {
          int area;
          area = width*height;
          return area;
         }
         public static void main(String[] args) {
               Rectangle myRect = new Rectangle();
                  myRect.width = 40;
                  myRect.height = 50;
                  System.out.println("myRect's area is " + myRect.area());
         }
}