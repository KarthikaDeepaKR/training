/*
    Question :
	
	demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects
*/

/*
    Requirements : To demonstrate inheritance,overloading,overriding using Animal,Dog,Cat,Snake classes.
*/

/*
    Entities : public class Animal
               public class Dog
			   public class Cat
			   public class Snake
*/

/*
    Method Signature : public void action(String name, String color)
                       public void action(String name)
					   public void bark()
					   public void action()
					   public void hiss()
                       public void meow()
					   public static void main(String args[])
*/

/*
    Jobs to be done : 1. Inside class Animal variables name and color are declared.
                      2. A method action() is defined with two parameters.(Method Overloading)
                      3. name and color is displayed inside this function.
                      4. A methos action() is defined with one parameter.(Method Overloading)
                      5. name is displayed inside this function.
                      6. Inside main function an object animal is created for Animal class.
                      7. The two methods are called with suitable arguments.
                      8. A class Dog is created which extends the class Animal.(Inheritance)
                      9. Inside class Dog a method bark() is created.
                      10. A method action() is created in class Dog with no parameters.
                      11. Inside the main function of class Dog an object dog is created.
                      12. The method action() is called using object dog.(Method Overriding)
                      13. A class Cat is created which extends the class Animal.(Inheritance)
           			  14. Inside class Cat a method meow() is created.
					  15. A class Snake is created which extends the class Animal.(Inheritance)
					  16. Inside class Snake a method hiss() is created.
*/  					 
public class Animal {
    
       String name;
       String color;
	   
      public void action(String name, String color){
        System.out.println("Moving....." + name + color);
      }
	  public void action(String name){
        System.out.println("Moving....." + name);
      }
	  
	  public static void main(String[] args){
		  Animal animal = new Animal();
		  animal.action("Dog","black");
		  animal.action("Dog");
	  }
    }
    

    
    
   
    