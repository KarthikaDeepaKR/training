/* 
    Question :
	
    Change the following program to use compound assignments:
	
       class ArithmeticDemo {

            public static void main (String[] args){

                 int result = 1 + 2; // result is now 3
                 System.out.println(result);

                 result = result - 1; // result is now 2
                 System.out.println(result);

                 result = result * 2; // result is now 4
                 System.out.println(result);

                 result = result / 2; // result is now 2
                 System.out.println(result);

                 result = result + 8; // result is now 10
                 result = result % 7; // result is now 3
                 System.out.println(result);
            }
       }
*/

/*  
    Requirements : To change the program by using compound assignments
*/

/*  
    Entities : public class ArithmeticDemo
*/

/*  
    Method Signature : public static void main (String[] args)
*/

/*  
    Jobs to be done : 1. The variable result is initialized with value 3.
	                  2. Display the value of variable result.
					  3. use the compound assignment operator " -= ".
					  4. Display the value of variable result.
					  5. use the compound assignment operator " *= ".
					  6. Display the value of variable result.
					  7. use the compound assignment operator " /= ".
					  8. Display the value of variable result.
                      9. use the compound assignment operator " -+= ".
                      10. Display the value of variable result.
                      11. use the compound assignment operator " %= ".
                      12. Display the value of variable result.	
*/					  
	   
//Ans:

package com.training.java.exercise;
public class ArithmeticDemo {

    public static void main (String[] args){
        int result = 3;  //result is now 3
        System.out.println(result);

        result -= 1; // result is now 2
        System.out.println(result);

        result *= 2; // result is now 4
        System.out.println(result);

        result /= 2; // result is now 2
        System.out.println(result);

        result += 8; // result is now 10
		System.out.println(result);
		
        result %= 7; // result is now 3
        System.out.println(result);

    }
}	


/*
    Output:

    3 
    2
    4
    2
    3
*/