/*
    In the following program, called ComputeResult, what is the value of result after each numbered line executes?
*/

/* 
    Requirements : To display the result of the given program
*/ 

/*
    Entities : public class ComputeResult
*/

/*
    Method Signature : public static void main(String[] args)
*/

/*  Jobs to be done : 1. The variable original is intialized with value "software".
                      2. The object named result is created for class StringBuilder.
                      3. The variable index is intialized with original.indexOf('a').
                      4. The function setCharAt() is used using object result to set character
                      5. The character is inserted by using insert() function with object result.
                      6. The String is appended by using append() function wiht object result.
                      7. The value in result is displayed.
*/ 					  
package com.training.java.exercise;
public class ComputeResult {
    public static void main(String[] args) {
        String original = "software";
        StringBuilder result = new StringBuilder("hi");
        int index = original.indexOf('a');

/*1*/   result.setCharAt(0, original.charAt(0));
/*2*/   result.setCharAt(1, original.charAt(original.length()-1));
/*3*/   result.insert(1, original.charAt(4));
/*4*/   result.append(original.substring(1,4));
/*5*/   result.insert(3, (original.substring(index, index+2) + " ")); 

        System.out.println(result);
    }
}

/*
Answer:

si
se
swe
sweoft
swear oft
*/