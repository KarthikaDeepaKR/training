//package employee;
import programs.*;

public class Employee {
	
	public String name;
	public int id;
	public int salary;
	
	//Constructor with three parameters
	public Employee(String name,int id,int salary) {
		this.name = name;
		this.id = id;
		this.salary = salary;
	}
	
	//private constructor with two parameters
	private Employee(String name,int salary) {
		System.out.println(name + salary);
	}
	
	//Constructor with no parameters i.e. default constructor
	public Employee() {
		System.out.println(name );
		
	}
	
	//Object initializer
	{
		id = 100;
		
	}
	
	//Method definition
	public void printDetails() {
		System.out.println("Employee name:" + name + " \n " + "id:" + id + "\n" + "salary:" + salary);
	}

	//Main function
public static void main(String args[]){
	
Employee emp = new Employee("Rajini",200,300000);
emp.printDetails();
Employee emp1 = new Employee("Bahu",300);
emp1.printDetails();
Employee emp2 = new Employee();
Employee emp3 = new Employee("Devi",400);
Company company = new Company();
company.run1();                   //compile time error : private access in Company
Manager manager = new Manager();
manager.run();                   //compile time error : protected access in Manager
emp.run(); 
}
}