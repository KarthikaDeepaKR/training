/*
    Question : Create a program that reads an unspecified number of integer arguments from the command line and adds them together.
               For example, suppose that you enter the following: java Adder 1 3 2 10
               The program should display 16 and then exit. The program should display an error message if the user enters only one argument.
*/

/*  Requirements : To create a program that reads integer arguments.
                   To display the sum of the integer arguments from the command line.
*/

/*  
    Entities : public class AdderDemo 
*/

/*  
    Method Signature : public static void main(String[] args)
 */
 
/*  
    Jobs to be done : 1.Length of the arguments is checked whether it is less than 2.
                      2.If the length is less than 2 ,the message " Enter two command-line arguments" is displayed.
                      3.If the length is not less than 2 variable sum is initialized with value 0.
                      4.for loop is created with i=0,i<args.length(),i++.
                      5.Again sum value is changed by sum + Integer.valueOf(args[i]).intValue().
                      8.Value in variable sum is displayed.		
*/
	
	
//Ans:
package com.training.java.exercise;
public class AdderDemo {
    public static void main(String[] args) {
	int numArgs = args.length;

	//this program requires at least two arguments on the command line
        if (numArgs < 2) {
            System.out.println("This program requires two command-line arguments.");
        } else {
	    int sum = 0;

	    for (int i = 0; i < numArgs; i++) {
                sum += Integer.valueOf(args[i]).intValue();
	    }

	    //print the sum
            System.out.println(sum);
        }
    }
}
